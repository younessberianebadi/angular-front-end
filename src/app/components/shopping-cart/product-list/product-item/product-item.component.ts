import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/models/product';
import { CartService } from 'src/app/services/cart.service';
import { MessengerService } from 'src/app/services/messenger.service';
// import { MessengerService } from 'src/app/services/messenger.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  
  @Input()
  product!: Product;

  constructor(
    private msg: MessengerService,
    private cartService: CartService
    ) { }

  ngOnInit(): void {
  }

  // handleAddToCart(){
  //   this.msg.sendMsg(this.productItem)
  // }

  addToCart(product: Product) {
    this.cartService.addToCart(this.product).subscribe(
      () => {
        this.msg.sendMsg(this.product)
      }
    )
    this.cartService.getItems()
    // console.log(this.cartService.getTotal())
  }

}
