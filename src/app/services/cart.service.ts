import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { cartUrl } from '../config/api';
import { Item } from '../models/item';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  items: Item[] = [];
  total = 0
  
  addToCart(product: Product): Observable<any> {
    
    // let productExists = false

    // for(let i in this.items){
    //   if(this.items[i].name === product.name){
    //     this.items[i].quantity += 1;
    //     this.total += product.price;
    //     productExists = true
    //     break;
    //   }
    // }

    // if(!productExists){
    //   this.items.push(new Item(product.name, product.id, product.price, 1));
    //   this.total += product.price;
    // }
    this.items.push(new Item(0, product, 1));
    this.total += product.price;
    return this.http.post(cartUrl, { product })

  }

  getItems(): Observable<Item[]> {
    return this.http.get<Item[]>(cartUrl);
  }

  getTotal() {
    this.items.forEach(item => {
      this.total += (item.price * item.quantity)
    })
    return this.total;
  }

  clearCart() {
    this.items = [];
    this.total = 0
    return this.items;
  }

  constructor(private http: HttpClient) { }
}
