import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/models/item';
import { Product } from 'src/app/models/product';
import { CartService } from 'src/app/services/cart.service';
import { MessengerService } from 'src/app/services/messenger.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartItems$ = this.cartService.getItems();

  get cartItem$(){
    return this.cartService.getItems();
  }
  cartTotal = 0

  clearCart(){
    this.cartService.clearCart()
  }

  get total()
  {
    return this.cartService.getTotal();
  }
  
  constructor(
    private msg: MessengerService,
    private cartService: CartService
    ) { 

    }
    

  ngOnInit(): void {
  }

}
