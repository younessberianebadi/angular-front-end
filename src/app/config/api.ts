import { environment } from "src/environments/environment";

export const baseUrl = environment.production ? 'api-service' : 'http://localhost:3004'
export const productsUrl = baseUrl + '/products'
export const cartUrl = baseUrl + '/cart'