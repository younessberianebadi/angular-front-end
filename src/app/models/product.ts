
export class Product {
    id: number;
    name: string;
    description: string;
    price: number;
    imageUrl: string;

    constructor(id: number, name: string, description: string = '', price: number = 0, imageUrl: string = 'https://static.thenounproject.com/png/220984-200.png'){
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
    }

}
