import { Product } from "./product";

export class Item {

    id: number
    name: string;
    productId: number;
    price: number;
    quantity: number


    constructor(id: number, product: Product, quantity: number = 1){

        this.id = id;
        this.name = product.name;
        this.productId = product.id;
        this.price = product.price;
        this.quantity = quantity;
    }
}
